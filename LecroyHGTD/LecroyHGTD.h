#ifndef LECROYHGTD_H
#define LECROYHGTD_H

#include <stdlib.h>
#include <string>
#include <vector>
#include <cstdint>
#include <map>

class LecroyHGTDData;

/**
 * LecroyHGTD is a class to read the Lecroy scope used by HGTD compatible with MaltaSW.
 * This is based on code by steve.sharples@nottingham.ac.uk and 
 * the WavePro Remote Control Manual from January 2002.
 * It uses a custom protocol based on the IEEE 488.2 standard over TCP sockets.
 * 
 * The constructor allocates the memory used for the header.
 * LecroyHGTD::Connect opens a TCP socket with the Scope.
 * It is possible to change which channels are enabled with LecroyHGTD::SetChan.
 * For convenience the method LecroyHGTD::GetEnabledChannels returns a list 
 * of the enabled channels (from 1 to 4). 
 * It is also possible to enable channels by using the LecroyHGTD::Parse method.
 * 
 * After selecting the channels to be enabled, the method LecroyHGTD::Configure
 * is required to change the settings of the scope for maximum throughput.
 * LecroyHGTD::Start enables the data acquisition of single waveforms that will
 * be stored in the memory of the scope. The maximum number of waveforms that 
 * can be stored is 4096.
 * LecroyHGTD::Stop disables the data acquisition and allows the user to transfer
 * the contents of the memory to this class with the method LecroyHGTD::Read.
 * 
 * The description of the waveform is stored in multiple methods.
 * - LecroyHGTD::GetWaveArray1: Length in bytes of the data array
 * - LecroyHGTD::GetWaveArrayCount: The number of data points in the data array
 * - LecroyHGTD::GetNomSubArrayCount: The segment index
 * - LecroyHGTD::GetHorizInterval: Sampling interval in units of LecroyWave::GetHorUnits
 * - LecroyHGTD::GetHorizOffset: Number of seconds between the trigger and the first sample
 * - LecroyHGTD::GetVerticalOffset: Offset applied to the data ( sample = GAIN * data - OFFSET )
 * - LecroyHGTD::GetVerticalGain: Gain applied to the data ( sample = GAIN * data - OFFSET )
 * - LecroyHGTD::GetTriggerTime: Time of the trigger
 * - LecroyHGTD::GetHorUnit: Units of the horizontal axis
 * - LecroyHGTD::GetVerUnit: Units of the vertical axis
 *
 * @verbatim
   
   LecroyHGTD * scope = new LecroyHGTD();
   scope->Connect("192.168.0.100");
   
   scope->SetChan(1,true);
   scope->Parse("Enable_Channel: 3");

   vector<uint32_t> enabled = scope->GetEnabledChannels();

   while(true){
     scope->Start();
     this_thread::sleep(chrono::seconds(1));
     scope->Stop();
     scope->Read();
    
     for(auto chan: enabled){
       cout << "scope settings: " << endl
            << " wave_array_1:      " << scope->GetWaveArray1(chan) << endl
	    << " wave_array_count:  " << scope->GetWaveArrayCount(chan) << endl
	    << " nom_subarray_count:" << scope->GetNomSubArrayCount(chan) << endl
	    << " horiz_interval:    " << scope->GetHorizInterval(chan) << endl
	    << " horiz_offset:      " << scope->GetHorizOffset(chan) << endl
	    << " vertical_offset:   " << scope->GetVerticalOffset(chan) << endl
	    << " vertical_gain:     " << scope->GetVerticalGain(chan) << endl
	    << " trigger_time:      " << scope->GetTriggerTime(chan) << endl;

       vector<uint8_t> * bytes = scope->GetBytes(chan);
       uint32_t length = scope->GetLength();
     }
   }

   delete scope;

   @endverbatim
 *
 * @brief Control and read-out of the HGTD's Lecroy scope
 * @author Carlos.Solans@cern.ch
 * @data August 2021
 **/
class LecroyHGTD{

 public:
  
  static const uint32_t MAX_NUM_EVENTS     = 4096;
  static const uint8_t LECROY_EOI_FLAG     = 0x01;
  static const uint8_t LECROY_SRQ_FLAG     = 0x08;
  static const uint8_t LECROY_CLEAR_FLAG   = 0x10;
  static const uint8_t LECROY_LOCKOUT_FLAG = 0x20;
  static const uint8_t LECROY_REMOTE_FLAG  = 0x40;
  static const uint8_t LECROY_DATA_FLAG    = 0x80;

  /**
   * Create a new LecroyHGTD object. Allocate memory for the communication.
   **/
  LecroyHGTD();

  /**
   * Close the socket if empty. Clear the memory allocated for the communication.
   **/
  ~LecroyHGTD();

  /**
   * Set the verbose flat. 
   * @param enable Increase the verbosity of the class if true.
   **/
  void SetVerbose(bool enable);

  /**
   * Get the verbose flag. 
   * @return Increase the verbosity of the class if true.
   **/
  bool GetVerbose();

  /**
   * Set the time in milliseconds to wait for packet to arrive before timeout
   * @param millis time in milliseconds
   **/
  void SetTimeout(uint32_t milis);
  
  /**
   * Enable or disable Nagle's algorithm
   * @param enable Disable Nagle's algorithm if false
   **/
  void SetNagle(bool enable);

  /**
   * Connect to a scope with a TCP socket.
   * @param ip_address the IP address of the scope.
   * @param port the port to connect to.
   * @return true upon successful connection.
   **/
  bool Connect(std::string ip_address, uint16_t port=1861);

  /**
   * Set the enable flag a channel. No data will be available if flag is off.
   * @param chan The channel to enable/disable (from 1 to 4)
   * @param enable Enable the channel if true.
   **/
  void SetChan(uint32_t chan, bool enable);

  /**
   * Set the enable flag a channel. No data will be available if flag is off.
   * @param chan The channel to enable/disable (from 1 to 4)
   * @param enable Enable the channel if true.
   **/
  bool GetChan(uint32_t chan);

  /**
   * Commodity method to get the list of enabled channels (from 1 to 4).
   * @return Vector of the enabled channels
   **/
  std::vector<uint32_t> GetEnabledChannels();

  /**
   * Commodity method to change the configuration through a string.
   * Valid configurations are:
   * - ENABLE_CHANNEL: N
   * @param config Configuration string
   **/
  void Parse(std::string config);

  /**
   * Configure the scope for running
   **/
  void Configure();

  /**
   * Start the data acquisition of the scope
   **/
  void Start();

  /**
   * Stop the data acquisition of the scope
   **/
  void Stop();

  /**
   * Read the data out from the scope.
   * It is necessary to call LecroyHGTD::Stop first.
   * To resume the data acquisition call LecroyHGTD::Start.
   **/
  void Read();

  /**
   * Close the TCP socket.
   **/
  void Disconnect();

  /**
   * Get the length in bytes of the data array
   * @param chan channel number 
   * @return unsigned integer with the length in bytes of the data array
   **/
  uint32_t GetWaveArray1(uint32_t chan);

  /**
   * Get the number of data points in the data array
   * @param chan channel number 
   * @return unsigned integer with the number of data points in the data array
   **/
  uint32_t GetWaveArrayCount(uint32_t chan);

  /**
   * Get the segment index
   * @param chan channel number 
   * @return unsigned segment index
   **/
  uint32_t GetNomSubArrayCount(uint32_t chan);

  /**
   * Get the sampling interval in units of LecroyWave::GetHorUnits
   * @param chan channel number 
   * @return float sampling interval
   **/
  float GetHorizInterval(uint32_t chan);

  /**
   * Get the number of seconds between the trigger and the first sample
   * @param chan channel number 
   * @return double seconds between trigger and first sample
   **/
  double GetHorizOffset(uint32_t chan);
  
  /**
   * Get the offset applied to the data ( GAIN * data - OFFSET )
   * @param chan channel number 
   * @return float the data offset
   **/
  float GetVerticalOffset(uint32_t chan);
  
  /**
   * Get the gain applied to the data ( GAIN * data - OFFSET )
   * @param chan channel number 
   * @return float the data gain
   **/
  float GetVerticalGain(uint32_t chan);
  
  /**
   * Get the time of the trigger
   * @param chan channel number 
   * @return double trigger time
   **/
  double GetTriggerTime(uint32_t chan);
  
  /**
   * Get the units of the horizontal axis
   * @param chan channel number 
   * @return string units of the horizontal axis
   **/
  std::string GetHorizUnit(uint32_t chan);
  
  /**
   * Get the units of the vertical axis
   * @param chan channel number 
   * @return string units of the vertical axis
   **/
  std::string GetVerticalUnit(uint32_t chan);

  /**
   * Get the number of samples per event (waveform)
   * @param chan channel number 
   * @return unsigned integer with the number of samples per event
   **/
  uint32_t GetNumSamples(uint32_t chan);

  /**
   * Get the number of events in the data
   * @param chan channel number 
   * @return unsigned integer with the number of events in the data 
   **/
  uint32_t GetNumEvents(uint32_t chan);

  /**
   * Get the byte array read from the scope
   * @param chan channel number 
   **/
  uint8_t * GetBytes(uint32_t chan);

  /**
   * Get the length of the byte array read from the scope
   * @param chan channel number 
   **/
  uint32_t GetLength(uint32_t chan);

  /**
   * Fill in a LecroyHGTDData object with the information from this channel and event id
   * @param data LecroyHGTDData object to be filled
   * @param chan channel number 
   * @param eventid sequence of the event to store
   **/
  void Fill(LecroyHGTDData* data, uint32_t chan, uint32_t eventid);

  /**
   * Set the abort flag that will be used to interrupt the read
   * @param cont pointer to a boolean value
   **/
  void SetAbortFlag(bool * cont);

  /**
   * Show the description of this oscilloscope on the screen.
   * Used for debugging purposes.
   **/
  void ShowDescription();

 private:

  bool * m_cont; //<! abort flag
  void WriteSock(std::string message);
  std::string ReadSock();
  int32_t ReadSock(uint8_t * buf);
  int32_t ReadSock(uint8_t * buf, uint32_t nbytes);
  std::string WriteAndRead(std::string message);
  
  uint32_t m_timeout; //<! Timeout in milliseconds
  int m_sockfd; //<! Socket used for TCP communication
  bool m_verbose; //<! Increase verbosity if enabled
  bool m_withbusy; //<! Send a busy signal if enabled
  std::map<uint32_t,std::vector<uint8_t> > m_bytes; //<! Byte array per channel 
  std::map<uint32_t,bool> m_chans; //<! Map of enabled channels

  std::map<uint32_t, uint32_t> m_wave_array_1; //!< length in bytes of the data array per channel
  std::map<uint32_t, uint32_t> m_wave_array_count; //!< number of points in the data array per channel
  std::map<uint32_t, uint32_t> m_nom_subarray_count;//!< segment index per channel
  std::map<uint32_t, float> m_horiz_interval; //!< sampling interval for time domain per channel
  std::map<uint32_t, double> m_horiz_offset; //!< seconds between trigger and first data point per channel
  std::map<uint32_t, float> m_vertical_offset; //!< offset applied to data ( GAIN * data - OFFSET ) per channel
  std::map<uint32_t, float> m_vertical_gain; //!< gain applied to data ( GAIN * data - OFFSET ) per channel
  std::map<uint32_t, double> m_trigger_time; //!< time of the trigger per channel
  std::map<uint32_t, std::string> m_horiz_unit; //!< time of the trigger per channel
  std::map<uint32_t, std::string> m_vertical_unit; //!< time of the trigger per channel
  
  std::map<uint32_t, std::vector<double> > m_trigger_times;
  std::map<uint32_t, std::vector<double> > m_trigger_offsets;

  std::string Trim(std::string str);

  std::string ToLower(std::string str);

};

#endif //LECROYHGTD
