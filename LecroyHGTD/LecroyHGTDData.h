#ifndef LECROYHGTDDATA_H
#define LECROYHGTDDATA_H

#include <string>
#include <vector>

/**
 * Custom implementation of the Lecroy Waveform
 * @author Carlos.Solans@cern.ch
 * @date August 2021
 **/
class LecroyHGTDData{

 public:

  /**
   * Create an empty LecroyHGTDData
   **/
  LecroyHGTDData();
  
  /**
   * Create a LecroyHGTDData object
   * by copying the contents from another one
   * @param a LecroyHGTDData reference 
   **/
  LecroyHGTDData(const LecroyHGTDData & data);
  
  /**
   * Clear the memory
   **/
  ~LecroyHGTDData();
  
  /**
   * Copy a LecroyHGTDData object
   * by copying the contents from another one
   * @param a LecroyHGTDData pointer
   **/
  void Copy(LecroyHGTDData * data);

  /**
   * Clear the sample and time arrays
   **/
  void Clear();

  /**
   * Get the number of samples in the waveform
   * @return the number of samples
   **/
  uint32_t GetNumSamples() const;

  /**
   * Get the time of the sample
   * @param pos position of the sample
   * @return double the time position of the sample
   **/
  double GetTime(uint32_t pos) const;
  
  /**
   * Get the value of the sample
   * @param pos position of the sample
   * @return double the value of the sample
   **/
  double GetSample(uint32_t pos) const;

  /**
   * Get the value of all the sample as a vector
   * @return a vector with a copy of all the sample values
   **/
  std::vector<double>& GetSamples();
  
  /**
   * Get the event number
   * @return unsigned integer with the event number
   **/
  uint32_t GetEventId();

  /**
   * Get the channel number
   * @return integer with the channel number
   **/
  uint32_t GetChannel();

  /**
   * Get the length in bytes of the data array
   * @return unsigned integer with the length in bytes of the data array
   **/
  uint32_t GetWaveArray1();

  /**
   * Get the number of data points in the data array
   * @return unsigned integer with the number of data points in the data array
   **/
  uint32_t GetWaveArrayCount();

  /**
   * Get the segment index
   * @return unsigned segment index
   **/
  uint32_t GetNomSubArrayCount();

  /**
   * Get the sampling interval in units of LecroyWave::GetHorUnits
   * @return float sampling interval
   **/
  float GetHorizInterval();

  /**
   * Get the number of seconds between the trigger and the first sample
   * @return double seconds between trigger and first sample
   **/
  double GetHorizOffset();
  
  /**
   * Get the offset applied to the data ( GAIN * data - OFFSET )
   * @return float the data offset
   **/
  float GetVerticalOffset();
  
  /**
   * Get the gain applied to the data ( GAIN * data - OFFSET )
   * @return float the data gain
   **/
  float GetVerticalGain();
  
  /**
   * Get the time of the trigger
   * @return double trigger time
   **/
  double GetTriggerTime();
  
  /**
   * Get the units of the horizontal axis
   * @return string units of the horizontal axis
   **/
  std::string GetHorUnit();
  
  /**
   * Get the units of the vertical axis
   * @return string units of the vertical axis
   **/
  std::string GetVerUnit();

  /**
   * Get the verbose flag
   * @return the verbose mode
   **/
  bool GetVerbose();

  /**
   * Set the event number
   * @param eventid unsigned integer with the event number
   **/
  void SetEventId(uint32_t eventid);

  /**
   * Set the channel number
   * @param channel integer with the channel number
   **/
  void SetChannel(uint32_t channel);

  /**
   * Set the length in bytes of the data array
   * @param length unsigned integer with the length in bytes of the data array
   **/
  void SetWaveArray1(uint32_t length);

  /**
   * Set the number of data points in the data array
   * @param count unsigned integer with the number of data points in the data array
   **/
  void SetWaveArrayCount(uint32_t count);

  /**
   * Set the segment index
   * @param index the segment index
   **/
  void SetNomSubArrayCount(uint32_t index);

  /**
   * Set the number of samples in the waveform
   * @param num_samples the number of samples
   **/
  void SetNumSamples(uint32_t num_samples);

  /**
   * Set the sampling interval in units of LecroyWave::GetHorUnits
   * @return float sampling interval
   **/
  void SetHorizInterval(float interval);

  /**
   * Set the number of seconds between the trigger and the first sample
   * @param offset double seconds between trigger and first sample
   **/
  void SetHorizOffset(double offset);
  
  /**
   * Set the offset applied to the data ( GAIN * data - OFFSET )
   * @param offset float the data offset
   **/
  void SetVerticalOffset(float offset);
  
  /**
   * Get the gain applied to the data ( GAIN * data - OFFSET )
   * @return float the data gain
   **/
  void SetVerticalGain(float gain);
  
  /**
   * Get the time of the trigger
   * @return double trigger time
   **/
  void SetTriggerTime(double time);
  
  /**
   * Set the units of the horizontal axis
   * @param unit units of the horizontal axis
   **/
  void SetHorUnit(std::string unit);
  
  /**
   * Set the units of the vertical axis
   * @param unit units of the vertical axis
   **/
  void SetVerUnit(std::string unit);

  /**
   * Set the verbose flag
   * @param enable the verbose mode
   **/
  void SetVerbose(bool enable);

 protected:

  bool m_verbose;
  uint32_t m_channel; //!< channel number
  uint32_t m_eventid; //!< event number
  uint32_t m_wave_array_1; //!< length in bytes of the data array
  uint32_t m_wave_array_count; //!< number of points in the data array
  uint32_t m_nom_subarray_count; //!< segment index
  uint32_t m_num_samples; //!< number of samples
  float m_horiz_interval; //!< sampling interval for time domain
  double m_horiz_offset; //!< seconds between trigger and first data point
  float m_vertical_offset; //!< offset applied to data ( GAIN * data - OFFSET )
  float m_vertical_gain; //!< gain applied to data ( GAIN * data - OFFSET )
  double m_trigger_time; //!< time of the trigger
  std::string m_horunit; //!< units of the horizontal axis
  std::string m_verunit; //!< units of the vertical axis
  std::vector<double> m_samples; //!< Sample array  

  /**
   * Convert 2 bytes into an unsigned short
   * @param data pointer to the byte array
   * @param pos position of the first byte
   * @param endian first byte is MSB if true, else first byte is LSB
   **/
  uint16_t bytes2short(uint8_t *data, unsigned pos, bool endian=0);
 
  friend class LecroyHGTDTree;
};


#endif
