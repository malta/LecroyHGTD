#ifndef LECROYHGTDMODULE_H
#define LECROYHGTDMODULE_H

#include "MaltaDAQ/ReadoutModule.h"
#include "LecroyHGTD/LecroyHGTD.h"
#include "LecroyHGTD/LecroyHGTDTree.h"

#include <string>
#include <thread>
#include <map>

/**
 * LecroyHGTDModule is an implementation of a ReadoutModule for the test-beam 
 * data-taking application (MaltaMultiDAQ) to control the Lecroy scope for HGTD.
 * LecroyHGTDModule can be loaded dynamically through the ModuleLoader as a ReadoutModule object.
 *
 * @verbatim

   ReadoutModule * malta2 = ModuleLoader::getInstance()->newReadoutModule("LecroyHGTD","LecroyHGTDModule",
                                                                          "PLANE_1","192.168.200.10","");
																		 
   @endverbatim
 * 
 * Malta2Module connects to the MALTA FPGA on creation, and configures it
 * for operation with Malta2Module::Configure. 
 * The configuration is steered from the ReadoutConfig object that is 
 * loaded from a configuration text file through ConfigParser.
 * 
 * @verbatim

   [PLANE 1]
   type: HGTD
   wafer: W4R10
   address: 192.168.200.10
   @endverbatim
 *
 *
 *
 * @brief LecroyHGTD ReadoutModule for MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @date August 2021
 **/

class LecroyHGTDModule: public ReadoutModule{
  
 public:
  
  /**
   * @brief: create a malta2 module
   * @param name the module name
   * @param address the ipbus connection string
   * @param outdir the output directory
   **/
  LecroyHGTDModule(std::string name, std::string address, std::string outdir);

  /**
   * @brief: delete pointers
   **/
  ~LecroyHGTDModule();
  
  /**
   * @brief Configure MALTA2 for data taking
   **/
  void Configure();
  
  /**
   * @brief Start data acquisition thread
   * @param run the run number as a string
   * @param usePath use the path 
   **/
  void Start(std::string run, bool usePath = false);

  /**
   * @brief Stop the data acquisition thread
   **/
  void Stop();

  /**
   * @brief The data acquisition loop
   **/
  void Run();

  /**
   * @brief get the L1A from the FPGA
   * @return L1A 
   **/
  uint32_t GetNTriggers();

  /**
   * @brief enable fast signal
   **/
  void EnableFastSignal(){};

  /**
   * @brief: disable fast signal
   **/
  void DisableFastSignal(){};

 private:
  
  LecroyHGTD * m_lecroy;
  LecroyHGTDTree * m_ntuple;

};

#endif

