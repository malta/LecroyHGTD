#ifndef LECROYHGTDTREE_H
#define LECROYHGTDTREE_H

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include <string>
#include "LecroyHGTD/LecroyHGTDData.h"
#include <chrono>

/**
 * @brief Class to store LecroyHGTD data in a ROOT tree
 **/
class LecroyHGTDTree{

 public:
  /**
   * @brief Construct and empty PicoTDCTree
   **/
  LecroyHGTDTree();
  
  /**
   * @brief Close ROOT file if open
   **/
  ~LecroyHGTDTree();
  
  /**
   * @brief Open ROOT file with given options
   * @param filename absolute path to the file
   * @param options ROOT options for new file
   **/
  void Open(std::string filename,std::string options);

  /**
   * @brief Close the ROOT file
   **/
  void Close();

  /**
   * @brief Add the current values to the Tree.
   * Set the values with PicoTDCTree::Set.
   **/
  void Fill();

  /**
   * @brief Set the values for the current event.
   * @param chan The channel number
   * @param data The PicoTDCData containing the data.
   **/
  void Set(uint32_t chan, LecroyHGTDData * data);

  /**
   * @brief Set the run number
   * @param run the runnumber
   **/
  void SetRunNumber(uint32_t run);

  /**
   * @brief Get the LecroyHGTD
   * @param chan The channel number
   * @return a pointer to the LecroyHGTDData
   **/
  LecroyHGTDData * Get(uint32_t chan);
  
  /**
   * @brief Get the run number
   * @return the run number
   **/
  uint32_t GetRunNumber();

  /**
   * @brief Get the next event from the Tree
   * @return different to 0 if failed
   **/
  int Next();

  /**
   * @brief Get the file containing the Tree.
   * @return TFile containing the Tree
   **/
  TFile* GetFile();

 private:
  
  TFile * m_file;
  TTree * m_tree;
  uint32_t m_run;
  LecroyHGTDData m_data_1;
  LecroyHGTDData m_data_2;
  LecroyHGTDData m_data_3;
  LecroyHGTDData m_data_4;
  uint64_t m_entry;
};

#endif

