1) GETTING THE LECROY DATA


Original program demonstrating simplified communication: 'TCP/IP control of LeCroy DSO by Steve D. Sharples, May 2003'
There are three programs :

lecroy_DAQ.cpp - the main program with all the commands for communication and data transfer
lecroy_tcp.cpp	 - the main library
lecroy_tcp.h	 - the header


SETTING UP & COMPILING
----------------------

You'll need to
1) have an ethernet cable connection with a stable IP adress 
2) edit lecroy_tcp.h to put this IP address

Hopefully, you should just be able to type "make" and the code will compile.


RUNNING
-------
Just run ./lecroy_example

The program will produce three files, one .dat with the waveforms encoded in binary, and two .txt with all the information needed for decoding (amplitude/time intervals and offsets, number of events and absolute trigger time) 

When you want to stop, just press ctrl+C. The program will first read-out the data of the oscilloscope and then exit safely

SOME COMMENTS
-------------
- current version of the program does not set the number of segments or number of points for the waveform. This needs to be configured from the oscilloscope itself

- a debug version is available. On the main program, uncomment #define WITH_DEBUG in line 55


!!!What the program does not do (yet)!!!
----------------------------------------

- synchronization with sps cycle
- does not get info from 2 oscilloscopes

2) ONLINE MONITORING

There are two python scripts based on the original testbeam monitoring codes (dataReader.py and dataMonitor.py):
LecroyReader.py  - decoding and extracting waveforms and relevant information. 
LecroyMonitor.py - plotting online as cycles update

RUNNING
-------

Just run python LecroyMonitor.py <timestamp> 

-The timestamp can be found on the name of the .dat/.txt file
-The default directory for the raw data is expected to be "./Data". It can be changed from the LecroyReader.py
-The ABSOLUTE trigger times are not calculated online , but can be aqcuired from the trig_<>.txt file once aqcuisition is complete - (I don't see any real need for having them at the online level...)

Don't hesitate to contact me if you have any problems with the code! 
Email: christina.agapopoulou@cern.ch


