#include "LecroyHGTD/LecroyHGTD.h"
#include "LecroyHGTD/LecroyHGTDData.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <chrono>

using namespace std;

LecroyHGTD::LecroyHGTD(){
  m_cont=NULL;
  m_sockfd=1;
  m_verbose=false;
  m_timeout=5000000;
}

LecroyHGTD::~LecroyHGTD(){
  Disconnect();
  for(auto [key,value] : m_bytes){
    value.clear();
  }
  m_bytes.clear();
}

void LecroyHGTD::SetVerbose(bool enable){
  m_verbose=enable;
}

bool LecroyHGTD::GetVerbose(){
  return m_verbose;
}

void LecroyHGTD::SetTimeout(uint32_t milis){
  if(m_verbose){cout << "Set timeout: " << milis << " ms" << endl;}
  m_timeout=milis;
  struct timeval tv;
  tv.tv_sec = (int)milis/1000;
  tv.tv_usec = (int)(milis%1000)*1000;
  setsockopt(m_sockfd,SOL_SOCKET,SO_RCVTIMEO,&tv,sizeof(tv));
}
  
void LecroyHGTD::SetNagle(bool enable){
  if(m_verbose){cout << "Set Nagle's algorightm: " << enable << endl;}
  int nonagle = !enable;
  setsockopt(m_sockfd,IPPROTO_TCP, nonagle, (char *)&nonagle, sizeof(nonagle));
}
  
void LecroyHGTD::Parse(string config){
  if(m_verbose){cout << "Parse: " << config << endl;}
  //Split key and value by colon, omit first and last char
  size_t pos = config.find_first_of(':');
  string key = ToLower(Trim(config.substr(0, pos)));
  string val = Trim(config.substr(pos+1));
  //Take action depending on the key
  //if(key=="enable_channel"){ uint32_t chan=atoi(val.c_str()); SetChan(chan,true); }
}


void LecroyHGTD::SetChan(uint32_t chan, bool enable){
  if(m_verbose){cout << "SetChan: " << chan << " " << (enable?"enabled":"disabled") << endl;}
  m_chans[chan]=enable;
}


vector<uint32_t> LecroyHGTD::GetEnabledChannels(){
  if(m_verbose){cout << "GetEnabledChannels" << endl;}
  vector<uint32_t> vec;
  for(auto [key,val] : m_chans){ if(val==true){vec.push_back(key);}}
  return vec;
}

bool LecroyHGTD::Connect(std::string ip_address, uint16_t port){
  if(m_verbose){cout << "Connect" << endl;}

  //Resolve host address:
  struct hostent * host = gethostbyname2(ip_address.c_str(),AF_INET);
  if(host==NULL){
    cout << "Host not found: " << ip_address << endl; 
    return false;
  }

  //Create the socket
  m_sockfd = socket (AF_INET, SOCK_STREAM, 0);

  //Create the address
  struct sockaddr_in addr;
  bzero (&addr, sizeof (addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  //addr.sin_addr.s_addr = inet_addr(ip_address.c_str());
  memcpy(&addr.sin_addr, host->h_addr, host->h_length);

  if(m_verbose){ 
    cout << "Try to connect to scope: " << ip_address << endl;
  }
  
  //Connect to the scope: open a tcp socket  
  connect(m_sockfd, (struct sockaddr *)&addr, sizeof (addr));
  
  if (m_sockfd<0) {
    cout << "Could not connect to the scope address: " << ip_address << endl;
    return false;
  }
  cout << "Connected..." << endl;
  
  //Disable Nagle's algorithm
  SetNagle(false);
  
  //Set the timeout
  SetTimeout(500);
  
  return true;
}

void LecroyHGTD::Configure(){
  if(m_verbose){cout << "Configure" << endl;}

  //Identify the scope
  WriteAndRead("*IDN?\n");
  
  //Show me the description
  //ShowDescription();
  
  //Enable sequence mode acquisition
  WriteAndRead("SEQ?;WAIT;*OPC?\n");
   
  //Enable data only response to WF?
  WriteAndRead("COMM_HEADER OFF;WAIT;*OPC?\n");

  //setting communiction format to 
  //DEF9: fixed size for the header #9 followed by 9 numbers
  //WORD: Transmits the waveform data as 16-bit signed integers
  //BIN: BINARY encoding
  WriteAndRead("COMM_FORMAT DEF9,WORD,BIN;WAIT;*OPC?\n");

  //LSB first - bytes need to be reversed
  WriteAndRead("CORD LO;WAIT;*OPC?\n");    
  
  //check what channels are active
  for(uint32_t chan=1; chan<=4; chan++){
    ostringstream cmd;
    cmd << "C" << chan << ":TRA?;WAIT;*OPC?\n";
    string ret=WriteAndRead(cmd.str());
    m_chans[chan]=(ret.find("ON")!=string::npos?true:false);
    cout << "Channel: " << chan << " is " << (m_chans[chan]?"enabled":"disabled") << endl;
  }

  if(m_withbusy){
    WriteAndRead("COUT PULSE, 0.05 V;*OPC?\n");
  }
}

void LecroyHGTD::Start(){
  if(m_verbose){cout << "Start" << endl;}
  //could be replaced by *TRG/TRMD SINGLE
  WriteAndRead("TRMD SINGLE;WAIT;*OPC?\n"); 
}

void LecroyHGTD::Read(){
  if(m_verbose){cout << "Read" << endl;}

  ostringstream os;
                    
  //emit busy signal
  if(m_withbusy){
    WriteAndRead("COUT LEVEL, 0.05 V;*OPC?\n");
  }
	 
  //loop for active channels
  for(auto const& [chan,enable] : m_chans){
    if (enable==false){continue;}
    if (m_cont and *m_cont==false) break;
    
    //Inspect settings
    os.str("");
    os << "C" << chan << ":INSP? 'WAVE_ARRAY_1';";
    os << "C" << chan << ":INSP? 'WAVE_ARRAY_COUNT';";
    os << "C" << chan << ":INSP? 'NOM_SUBARRAY_COUNT';";
    os << "C" << chan << ":INSP? 'HORIZ_INTERVAL';";
    os << "C" << chan << ":INSP? 'HORIZ_OFFSET';";
    os << "C" << chan << ":INSP? 'VERTICAL_OFFSET';";
    os << "C" << chan << ":INSP? 'VERTICAL_GAIN';";
    os << "C" << chan << ":INSP? 'TRIGGER_TIME';";
    os << "C" << chan << ":INSP? 'VERTUNIT';";
    os << "C" << chan << ":INSP? 'HORUNIT';";
    os << "WAIT;*OPC?";

    string rt=WriteAndRead(os.str());
    size_t pos=0;
    while(pos!=string::npos){
      if(m_verbose) cout << "find next key pos=" << pos << endl;
      size_t pos2 = rt.find(";",pos);
      string rep = rt.substr(pos+1,pos2-pos-2);
      if(m_verbose) cout << "parse: " << rep << endl;
      size_t pos3 = rep.find(":");
      if(pos3==string::npos){break;}
      string key = Trim(rep.substr(0,pos3));
      string val = Trim(rep.substr(pos3+1));
      cout << "Parse: key=(" << key << "), val=(" << val << ")" << endl;
      if     (key.find("WAVE_ARRAY_1")      !=string::npos){ m_wave_array_1[chan]=atol(val.c_str());}
      else if(key.find("WAVE_ARRAY_COUNT")  !=string::npos){ m_wave_array_count[chan]=atol(val.c_str());}
      else if(key.find("NOM_SUBARRAY_COUNT")!=string::npos){ m_nom_subarray_count[chan]=atol(val.c_str());}
      else if(key.find("HORIZ_INTERVAL")    !=string::npos){ m_horiz_interval[chan]=atof(val.c_str());}
      else if(key.find("HORIZ_OFFSET")      !=string::npos){ m_horiz_offset[chan]=atof(val.c_str());}
      else if(key.find("VERTICAL_OFFSET")   !=string::npos){ m_vertical_offset[chan]=atof(val.c_str());}
      else if(key.find("VERTICAL_GAIN")     !=string::npos){ m_vertical_gain[chan]=atof(val.c_str());}
      else if(key.find("TRIGGER_TIME")      !=string::npos){ m_trigger_time[chan]=atof(val.c_str());}
      else if(key.find("HORUNIT")           !=string::npos){ m_horiz_unit[chan]=val;}
      else if(key.find("VERTUNIT")          !=string::npos){ m_vertical_unit[chan]=val;}
      pos=pos2+1;
    }

    //Request the waveform data
    //Might require multiple reads of the socket to reach the number of expected bytes
    os.str("");
    os << "C" << chan << ":WF? DAT1;WAIT;*OPC?\n";
    cout << "Request data: " << os.str() << endl;
    WriteSock(os.str());

    if(m_verbose){
      cout << "wave_array_1: " << m_wave_array_1[chan] << endl
	   << "wave_array_count: " << m_wave_array_count[chan] << endl
	   << "nom_subarray_count: " << m_nom_subarray_count[chan] << endl
	   << endl;
    }
    //Calculate the number of expected bytes
    //uint32_t expected_bytes=2*m_wave_array_count[chan]+19;
    uint32_t expected_bytes=m_wave_array_1[chan]+19;

    //Read the expected number of bytes into the bytes array
    cout << "Request data of size: " << expected_bytes << endl;
    m_bytes[chan].resize(expected_bytes);
    ReadSock(&m_bytes[chan][0],expected_bytes);
    cout << "Done reading" << endl;
    
    /*
    //get the trigger time: array of "TRIG_TIME TRIGGER_OFFSET\r\n"
    expected_bytes=nsegInCycle*40+66;//2*2*nsegInCycle*npoints*8+19;
    os.str("");
    os << "C" << chan << ":INSP? TRIGTIME;WAIT;*OPC?\n";
    WriteAndRead(os.str());

    //Convert them into trigger_times and trigger_offsets per channel 
    string stt(m_inbuf.data());
    istringstream itt(stt); 
    for(uint32_t i=0;i<3;i++){itt.ignore(100,'\n');}
    double tt=0.0;
    double to=0.0;
    m_trigger_times[chan].clear();
    m_trigger_offsets[chan].clear();
    while(itt.eof()){
      itt >> tt >> to;
      m_trigger_times[chan].push_back(tt);
      m_trigger_offsets[chan].push_back(to);
    }
    */


  }
    
  //emit busy signal
  if(m_withbusy){
    WriteAndRead("COUT LEVEL, 0.95 V;*OPC?\n");
    WriteAndRead("COUT PULSE, 0.05 V;*OPC?\n");
  }
  
}

void LecroyHGTD::Stop(){
  //"STOP BUTTON" command
  WriteAndRead("STOP;WAIT;*OPC?\n");
}

void LecroyHGTD::Disconnect(){
  if(m_sockfd==-1) return;
  if(m_verbose){cout << "Disconnect" << endl;}
  //WriteAndRead("STOP\n");
  close(m_sockfd);
  cout << "Disconnected..." << endl;
  m_sockfd=-1;
}

uint32_t LecroyHGTD::GetWaveArray1(uint32_t chan){
  return m_wave_array_1[chan];
}

uint32_t LecroyHGTD::GetWaveArrayCount(uint32_t chan){
  return m_wave_array_count[chan];
}

uint32_t LecroyHGTD::GetNomSubArrayCount(uint32_t chan){
  return m_nom_subarray_count[chan];
}

float LecroyHGTD::GetHorizInterval(uint32_t chan){
  return m_horiz_interval[chan];
}

double LecroyHGTD::GetHorizOffset(uint32_t chan){
  return m_horiz_offset[chan];
}

float LecroyHGTD::GetVerticalOffset(uint32_t chan){
  return m_vertical_offset[chan];
}

float LecroyHGTD::GetVerticalGain(uint32_t chan){
  return m_vertical_gain[chan];
}

double LecroyHGTD::GetTriggerTime(uint32_t chan){
  return m_trigger_time[chan];
}

string LecroyHGTD::GetHorizUnit(uint32_t chan){
  return m_horiz_unit[chan];
}

string LecroyHGTD::GetVerticalUnit(uint32_t chan){
  return m_vertical_unit[chan];
}

uint32_t LecroyHGTD::GetNumSamples(uint32_t chan){
  if(m_nom_subarray_count[chan]==0) return 0;
  return m_wave_array_count[chan]/m_nom_subarray_count[chan];
}

uint32_t LecroyHGTD::GetNumEvents(uint32_t chan){
  return m_nom_subarray_count[chan];
}

uint8_t * LecroyHGTD::GetBytes(uint32_t chan){
  return m_bytes[chan].data();
}

uint32_t LecroyHGTD::GetLength(uint32_t chan){
  return m_bytes[chan].size();
}

void LecroyHGTD::Fill(LecroyHGTDData* data, uint32_t chan, uint32_t event){
  if(event>GetNomSubArrayCount(chan)){return;}
  data->SetWaveArray1(GetWaveArray1(chan));
  data->SetWaveArrayCount(GetWaveArrayCount(chan));
  data->SetNomSubArrayCount(GetNomSubArrayCount(chan));
  data->SetHorizInterval(GetHorizInterval(chan));
  data->SetHorizOffset(GetHorizOffset(chan));
  data->SetVerticalOffset(GetVerticalOffset(chan));
  data->SetVerticalGain(GetVerticalGain(chan));
  data->SetTriggerTime(GetTriggerTime(chan));
  data->SetNumSamples(GetNumSamples(chan));
  data->GetSamples().clear();
  if(GetNumSamples(chan)==0) return;
  uint32_t num_samples=GetNumSamples(chan);
  uint32_t size=m_bytes[chan].size();
  double offset=GetVerticalOffset(chan);
  double gain=GetVerticalGain(chan);
  for(uint32_t s=0;s<num_samples;s++){
    if((2*num_samples*event+2*s+1)>size){break;}
    //uint32_t adc = 0;
    int16_t adc = 0;
    adc += ((0xFF & m_bytes[chan][2*num_samples*event+2*s+1]) << 8);
    adc += ((0xFF & m_bytes[chan][2*num_samples*event+2*s+0]) << 0);
    //data->GetSamples().push_back(adc);
    double sample = (gain*(float)adc)-offset;
    data->GetSamples().push_back(sample);
  }
}

void LecroyHGTD::SetAbortFlag(bool * cont){
  m_cont=cont;
}

void LecroyHGTD::ShowDescription(){
  string msg("INSPECT? WAVEDESC;WAIT;*OPC?\n");
  cout << "Request: ";
  for(uint32_t i=0;i<msg.size()-1;i++){cout << msg[i];}
  cout << endl;
  
  //Show me the description
  WriteSock(msg);

  SetTimeout(100);
  char str[10000];
  memset(str,0,10000);
  uint32_t len=ReadSock((uint8_t*)&str[0],3000);
  SetTimeout(5000000);

  cout << "Reply: " << len << endl;
  for(uint32_t i=0;i<len;i++){cout << str[i];}
  cout << endl;  
}

void LecroyHGTD::WriteSock(string msg){

  if(m_verbose) cout << "WriteSock" << endl;

  uint8_t header[8];
  header[0] = LECROY_DATA_FLAG;
  //header[0] += LECROY_REMOTE_FLAG;
  header[0] += LECROY_EOI_FLAG;
  header[1] = 1; //header version
  header[2] = 0; //sequence number
  header[3] = 0; //spare
  header[4] = (msg.size() & 0xFF000000) >> 24; //block size MSB
  header[5] = (msg.size() & 0x00FF0000) >> 16; //block size 
  header[6] = (msg.size() & 0x0000FF00) >>  8; //block size
  header[7] = (msg.size() & 0x000000FF) >>  0; //block size LSB 

  if(m_verbose) {
    cout << "Write header: 0x" << hex;
    for(auto byte: header){ cout << setw(2) << setfill('0') << (uint32_t) byte; }
    cout << dec << endl;
  }

  int32_t nb=write(m_sockfd, (char *) &header, sizeof(header));
  if(nb<(int32_t)sizeof(header)){ cout << "Cannot write header" << endl; }

  vector<uint8_t> buf;
  for(auto byte: msg){ buf.push_back(byte); }

  if(m_verbose) {
    cout << "Write message: 0x" << hex;
    for(auto byte: msg){ cout << setw(2) << setfill('0') << (uint32_t) byte; }
    cout << dec << endl;
  }
  
  nb=write(m_sockfd, buf.data(), buf.size());
  if(nb<(int32_t)buf.size()){ cout << "Cannot write msg" << endl; }
  if(m_verbose) cout << "WriteSock end" << endl;
}


string LecroyHGTD::ReadSock(){
  uint8_t buf[1000];
  uint32_t len=ReadSock(buf);
  string str;
  for(uint32_t i=0;i<len;i++){str.append(1,buf[i]);}
  return str;
}

int32_t LecroyHGTD::ReadSock(uint8_t * buf){
  
  if(m_verbose) cout << "ReadSock" << endl;
  
  //make sure there is something to read
  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(m_sockfd, &rfds);
  //set timeout
  struct timeval tval;
  tval.tv_sec = 1;//(int)m_timeout/1000;
  tval.tv_usec = 300000;//(int)(m_timeout%1000)*1000;
  
  //read the header
  int32_t nb=0;
  int32_t pos=0;
  uint8_t header[8];
  memset(&header, 0, sizeof(header));
  /*  
  while(pos<8){
    if(m_cont and *m_cont==false) {return 0;}
    int32_t result = select((m_sockfd+1), &rfds, NULL, NULL, &tval);
    if(result<0){cout << "Timeout waiting for the socket" << endl; return 0;}
    nb=read(m_sockfd, &header+pos, 8-pos);
    //cout << "read nb: " << nb << endl;
    if(nb<0){ cout << "failed to read header" << endl; return 0; }
    pos+=nb;
  }
  */
  auto t0 = std::chrono::steady_clock::now(); 
  while(pos<8){
    if(m_cont and *m_cont==false) {return 0;}
    FD_ZERO(&rfds);
    FD_SET(m_sockfd, &rfds);
    int32_t result = select((m_sockfd+1), &rfds, NULL, NULL, &tval);
    auto t1 = std::chrono::steady_clock::now(); 
    if(result==0){
      if(chrono::duration_cast<chrono::seconds>(t1-t0).count()>5000){
	cout << "Timeout waiting for data to be ready" << endl;
	return 0;
      }
      continue;
    }
    nb=read(m_sockfd, &header+pos, 8-pos);
    if(nb<0){ cout << "failed to read header" << endl; return 0; }
    pos+=nb;
  }
    
  if(m_verbose){
    cout << "Header: " << hex;
    for(uint32_t i=0;i<8;i++){ cout << setw(2) << setfill('0') << (uint32_t) header[i]; }
    cout << dec << endl;
  }

  int32_t length = 0;
  length +=  header[4] << 24;
  length +=  header[5] << 16;
  length +=  header[6] <<  8;
  length +=  header[7] <<  0;
  
  if(m_verbose) cout << "Read size: " << length << endl;

  pos=0;
  while(pos<length){
    nb=read(m_sockfd, buf+pos, ((length-pos)>2048?2048:length-pos));
    if(nb<0){ cout << "Cannot read reply" << " length:" << length << endl; break; }
    pos+=nb;
  }
    
  if(m_verbose){
    cout << "Read message: 0x" << length;
    for(uint32_t i=0;i<(uint32_t)pos;i++){cout << setw(2) << setfill('0') << (uint32_t) buf[i];}
    cout << endl;
  }
  
  return pos;
} 

int32_t LecroyHGTD::ReadSock(uint8_t * buf, uint32_t nbytes){
  if(m_verbose) cout << "Read nbytes: " << nbytes << endl;
  uint32_t tnb = 0;
  while(tnb<nbytes){
    if(m_cont and *m_cont==false) break;
    int32_t nb = ReadSock(buf+tnb);
    if(nb<0) break;
    tnb+=nb;
    //make sure the next block is ready for reading
    //if we fail reading the header we are fucked
    usleep(10);
  }
  return tnb;
}

string LecroyHGTD::WriteAndRead(string msg){
  cout << "Request: ";
  for(uint32_t i=0;i<msg.size()-1;i++){cout << msg[i];}
  cout << endl;
  
  WriteSock(msg);
  string str=ReadSock();

  cout << "Reply: ";
  for(uint32_t i=0;i<str.size();i++){cout << str[i];}
  cout << endl;
  return str;
}

string LecroyHGTD::Trim(string str){
  size_t first = str.find_first_not_of(' ');
  if (first == std::string::npos) return "";
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last-first+1));
}

string LecroyHGTD::ToLower(string str){
  transform(str.begin(), str.end(), str.begin(), ::tolower);
  return str;
}
