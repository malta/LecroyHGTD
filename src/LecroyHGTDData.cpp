#include "LecroyHGTD/LecroyHGTDData.h"
#include <iostream>
#include <iomanip>

using namespace std;

LecroyHGTDData::LecroyHGTDData() {
  m_verbose            = 0;
  m_eventid            = 0;
  m_channel            = 0;
  m_wave_array_1       = 0;
  m_wave_array_count   = 0;
  m_nom_subarray_count = 0;
  m_horiz_interval     = 0.0f;
  m_horiz_offset       = 0.0;
  m_vertical_offset    = 0.0f;
  m_vertical_gain      = 0.0f;
  m_trigger_time       = 0.0; 
}

LecroyHGTDData::LecroyHGTDData(const LecroyHGTDData &w) {
  m_verbose            = w.m_verbose;
  m_eventid            = w.m_eventid;
  m_channel            = w.m_channel;
  m_wave_array_1       = w.m_wave_array_1;
  m_wave_array_count   = w.m_wave_array_count;
  m_nom_subarray_count = w.m_nom_subarray_count;
  m_horiz_interval     = w.m_horiz_interval;
  m_horiz_offset       = w.m_horiz_offset;
  m_vertical_offset    = w.m_vertical_offset;
  m_vertical_gain      = w.m_vertical_gain;
  m_trigger_time       = w.m_trigger_time;
  m_samples            = w.m_samples;
  m_num_samples        = w.m_num_samples;
}

LecroyHGTDData::~LecroyHGTDData() {
  Clear();
}

void LecroyHGTDData::Copy(LecroyHGTDData * data) {
  m_verbose            = data->GetVerbose();
  m_eventid            = data->GetEventId();
  m_channel            = data->GetChannel();
  m_wave_array_1       = data->GetWaveArray1();
  m_wave_array_count   = data->GetWaveArrayCount();
  m_nom_subarray_count = data->GetNomSubArrayCount();
  m_horiz_interval     = data->GetHorizInterval();
  m_horiz_offset       = data->GetHorizOffset();
  m_vertical_offset    = data->GetVerticalOffset();
  m_vertical_gain      = data->GetVerticalGain();
  m_trigger_time       = data->GetTriggerTime();
  m_samples            = data->GetSamples();
  m_num_samples        = data->GetNumSamples();
}

void LecroyHGTDData::Clear() {
  m_samples.clear();
}

uint32_t LecroyHGTDData::GetNumSamples() const{
  return m_num_samples;
}

double LecroyHGTDData::GetTime(uint32_t pos) const {
  return m_horiz_interval*pos+m_horiz_offset;
}

double LecroyHGTDData::GetSample(uint32_t pos) const {
  if(pos>m_samples.size()) return 0;
  return m_samples[pos];
}

vector<double>& LecroyHGTDData::GetSamples(){
  return m_samples;
}

uint32_t LecroyHGTDData::GetEventId(){
  return m_eventid;
}

uint32_t LecroyHGTDData::GetChannel(){
  return m_channel;
}

uint32_t LecroyHGTDData::GetWaveArray1(){
  return m_wave_array_1;
}

uint32_t LecroyHGTDData::GetWaveArrayCount(){
  return m_wave_array_count;
}

uint32_t LecroyHGTDData::GetNomSubArrayCount(){
  return m_nom_subarray_count;
}

float LecroyHGTDData::GetHorizInterval(){
  return m_horiz_interval;
}

double LecroyHGTDData::GetHorizOffset(){
  return m_horiz_offset;
}
  
float LecroyHGTDData::GetVerticalOffset(){
  return m_vertical_offset;
} 

float LecroyHGTDData::GetVerticalGain(){
  return m_vertical_gain;
}
  
double LecroyHGTDData::GetTriggerTime(){
  return m_trigger_time;
}
  
string LecroyHGTDData::GetHorUnit(){
  return m_horunit;
}
  
string LecroyHGTDData::GetVerUnit(){ 
  return m_verunit;
}

bool LecroyHGTDData::GetVerbose(){
  return m_verbose;
}

void LecroyHGTDData::SetEventId(uint32_t eventid){
  m_eventid = eventid;
}

void LecroyHGTDData::SetChannel(uint32_t channel){
  m_channel = channel;
}

void LecroyHGTDData::SetWaveArray1(uint32_t length){
  m_wave_array_1 = length;
}

void LecroyHGTDData::SetWaveArrayCount(uint32_t count){
  m_wave_array_count = count;
}

void LecroyHGTDData::SetNomSubArrayCount(uint32_t index){
  m_nom_subarray_count = index;
}

void LecroyHGTDData::SetNumSamples(uint32_t num_samples){
  m_num_samples = num_samples;
}

void LecroyHGTDData::SetHorizInterval(float interval){
  m_horiz_interval = interval;
}

void LecroyHGTDData::SetHorizOffset(double offset){
  m_horiz_offset = offset;
}
  
void LecroyHGTDData::SetVerticalOffset(float offset){
  m_vertical_offset = offset;
}
  
void LecroyHGTDData::SetVerticalGain(float gain){
  m_vertical_gain = gain;
}

void LecroyHGTDData::SetTriggerTime(double time){
  m_trigger_time = time;
}
  
void LecroyHGTDData::SetHorUnit(std::string unit){
  m_horunit = unit;
}

void LecroyHGTDData::SetVerUnit(std::string unit){
  m_verunit = unit;
}

void LecroyHGTDData::SetVerbose(bool enable){
  m_verbose = enable;
}


