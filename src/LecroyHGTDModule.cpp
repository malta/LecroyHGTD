#include "LecroyHGTD/LecroyHGTDModule.h"
#include <iostream>
#include <sstream>
#include <chrono>

using namespace std;

DECLARE_READOUTMODULE(LecroyHGTDModule)

LecroyHGTDModule::LecroyHGTDModule(std::string name, std::string address, std::string outdir):   
  ReadoutModule(name,address,outdir){
  m_lecroy = new LecroyHGTD();
  m_lecroy->Connect(address);
  m_ntuple = 0;
}

LecroyHGTDModule::~LecroyHGTDModule(){
  delete m_lecroy;
  if(m_ntuple) delete m_ntuple;
}

void LecroyHGTDModule::Configure(){
  cout << m_name << "Configure" << endl;
  ReadoutConfig * config = GetConfig();
  if(config){
    for(auto key : config->GetKeys()){
      m_lecroy->Parse(key);
    }
  }
  cout << m_name << "Actually configure the LecroyHGTD" << endl;
  m_lecroy->Configure();
} 

void LecroyHGTDModule::Start(std::string run, bool usePath){

  string fname;
  cout << m_name << ": Start run " << run << endl;
  if(!usePath){
    ostringstream os;
    os << m_outdir << "/" << "run_" << run << ".root";
    fname = os.str();
  }else{
    fname = run;
  }
    
  //Create ntuple
  cout << m_name << ": Create ntuple" << endl;
  if(m_ntuple) delete m_ntuple;
  m_ntuple = new LecroyHGTDTree();
  m_ntuple->Open(fname,"RECREATE");
  string srun=run.substr(0, run.find('.'));
  m_ntuple->SetRunNumber(atoi(srun.c_str()));
  cout << m_name << ": Prepare for run"<< endl;
  
  cout << m_name << ": Create data acquisition thread" << endl;
  m_thread = thread(&LecroyHGTDModule::Run,this);
  m_running = true;
  cout << m_name << ": Data acquisition thread running" << endl;

  m_ntuple->GetFile()->Write();
}

void LecroyHGTDModule::Stop(){
  cout << m_name << ": Stop" << endl;
  m_cont=false;
  cout << m_name << ": Join data acquisition thread" << endl;
  m_thread.join();
  cout << m_name << ": Close tree" << endl;
  m_ntuple->Close();
}

void LecroyHGTDModule::Run(){
  uint32_t eventid=0;
  m_cont=true;
  
  //Make sure the Lecroy aborts also when needed
  m_lecroy->SetAbortFlag(&m_cont);
 
  //Loop until the run is stopped
  while(m_cont){
    cout << m_name << ": Run: Start aqcuisition" << endl;
    m_lecroy->Start();
    cout << m_name << ": Run: Stop aqcuisition" << endl;
    m_lecroy->Stop();
    cout << m_name << ": Run: Read all data out" << endl;
    m_lecroy->Read();
    if(!m_cont) break; 
    cout << m_name << ": Run: Extract the events" << endl;
    auto t0 = std::chrono::steady_clock::now(); 
    for(uint32_t event=0;event<m_lecroy->MAX_NUM_EVENTS;event++){
      bool fill=false;
      for(auto& chan: m_lecroy->GetEnabledChannels()){
	if(event==0) cout << "Chan: " <<chan << ", NumEvents: " << m_lecroy->GetNomSubArrayCount(chan) << endl;	
	if(event>m_lecroy->GetNumEvents(chan)) continue;
	m_ntuple->Get(chan)->SetEventId(eventid);
	m_lecroy->Fill(m_ntuple->Get(chan),chan,event);
	fill=true;
      }
      if(!fill) continue;
      m_ntuple->Fill();
      eventid++;
    }
    auto t1 = std::chrono::steady_clock::now(); 
    cout << m_name << ": Run: Filling took " 
	 << chrono::duration_cast<chrono::milliseconds>(t1-t0).count()
	 << " ms" << endl;
    
  }
}

uint32_t LecroyHGTDModule::GetNTriggers(){
  return 0;
}
