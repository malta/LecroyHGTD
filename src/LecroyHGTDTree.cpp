#include "LecroyHGTD/LecroyHGTDTree.h"
#include <iostream>
#include <fstream>
using namespace std;

LecroyHGTDTree::LecroyHGTDTree(){
  m_run=0;
  m_entry=0;
  m_file=0;
}

LecroyHGTDTree::~LecroyHGTDTree(){
  if(m_file) delete m_file;
}

void LecroyHGTDTree::Open(string filename, string options){
  m_file = TFile::Open(filename.c_str(),options.c_str());
  m_tree = new TTree("LecroyHGTD","DataStream");
  m_tree->Branch("run",&m_run,"run/i");
  m_tree->Branch("eventid",&m_data_1.m_eventid,"eventid/i");
  m_tree->Branch("trigger_time",&m_data_1.m_trigger_time,"trigger_time/D");
  m_tree->Branch("num_samples_1",&m_data_1.m_num_samples,"num_samples_1/i");
  m_tree->Branch("num_samples_2",&m_data_2.m_num_samples,"num_samples_2/i");
  m_tree->Branch("num_samples_3",&m_data_3.m_num_samples,"num_samples_3/i");
  m_tree->Branch("num_samples_4",&m_data_4.m_num_samples,"num_samples_4/i");
  m_tree->Branch("samples_1",&m_data_1.m_samples);
  m_tree->Branch("samples_2",&m_data_2.m_samples);
  m_tree->Branch("samples_3",&m_data_3.m_samples);
  m_tree->Branch("samples_4",&m_data_4.m_samples);
  m_entry = 0;
}

void LecroyHGTDTree::Set(uint32_t chan, LecroyHGTDData * data){
  switch(chan){
  case 1: m_data_1.Copy(data);
  case 2: m_data_2.Copy(data);
  case 3: m_data_3.Copy(data);
  case 4: m_data_4.Copy(data);
  }
}

void LecroyHGTDTree::SetRunNumber(uint32_t run){
  m_run=run;
}

void LecroyHGTDTree::Fill(){
  m_tree->Fill();  
}

int LecroyHGTDTree::Next(){
  int ret=m_tree->GetEntry(m_entry);
  m_entry++;
  return ret;
}

LecroyHGTDData * LecroyHGTDTree::Get(uint32_t chan){
  switch(chan){
  case 1: return &m_data_1;
  case 2: return &m_data_2;
  case 3: return &m_data_3;
  case 4: return &m_data_4;
  default: return &m_data_1;
}
}

uint32_t LecroyHGTDTree::GetRunNumber(){
  return m_run;
}

TFile* LecroyHGTDTree::GetFile(){
  return m_file;
}

void LecroyHGTDTree::Close(){
  m_file->cd();
  m_tree->Write();
  m_file->Close();
}
