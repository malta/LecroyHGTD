#include "LecroyHGTD/LecroyHGTDData.h"
 
#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <map>
#include <cmdl/cmdargs.h>
#include <algorithm>

using namespace std;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# decode_lecroywave             #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(   'v',"verbose","turn on verbose mode");
  CmdArgBool cReverse(   'r',"reverse","read the bytes backwards");
  CmdArgStr  cFilePath(  'f',"file","path","file to parse", CmdArg::isREQ);
  CmdArgStr  cFileFormat('F',"format","format","text,binary. Default text");
  CmdArgInt  cNumBytes(  'b',"bytes","bytes","number of bytes to read");
  CmdArgInt  cSkipBytes( 's',"skip","bytes","number of bytes to skip");
  CmdArgFloat cVerGain(  'g',"gain","gain","vertical gain");
  CmdArgFloat cVerOffset('o',"offset","offset","vertical offset");
  
  CmdLine cmdl(*argv,&cVerbose,&cFilePath,&cFileFormat,&cReverse,
	       &cNumBytes,&cSkipBytes,&cVerGain,&cVerOffset,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string format=(cFileFormat.flags()&CmdArg::GIVEN?cFileFormat:"text");

  cout << "Create new LecroyWave" << endl;
  LecroyHGTDData *wave=new LecroyHGTDData();

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes;

  cout << "Loop over file contents" << endl;

  if(format=="text"){
    ifstream fr(cFilePath);
    fr.seekg(0, std::ios::end);
    cout << "File size (bytes): " << fr.tellg() << endl;
    fr.seekg(0, std::ios::beg);
    char c1;
    char c2;
    map<char,uint32_t> c2n={
        {'0', 0}, {'1', 1}, {'2', 2}, {'3', 3}, {'4', 4},
        {'5', 5}, {'6', 6}, {'7', 7}, {'8', 8}, {'9', 9},
        {'A',10}, {'B',11}, {'C',12}, {'D',13}, {'E',14}, {'F',15},
        {'a',10}, {'b',11}, {'c',12}, {'d',13}, {'e',14}, {'f',15}
    };
    while(fr){
      fr >> c1 >> c2;
      if(fr.eof())break;
      uint8_t byte = c2n[c1]*16+c2n[c2];
      if(cVerbose) cout << "byte: 0x" << hex << (uint32_t) byte << dec << endl;
      bytes.push_back(byte);
    }
    fr.close();
  }else if(format=="binary"){
    ifstream fr(cFilePath, ios::binary);
    fr.seekg(0, std::ios::end);
    ifstream::pos_type pos = fr.tellg();
    cout << "File size (bytes): " << pos << endl;
    fr.seekg(0, std::ios::beg);
    bytes.resize(pos);
    fr.read((char*)&bytes[0],pos);
    fr.close();
  }

  if(cReverse){
    std::reverse(bytes.begin(),bytes.end());
  }

  uint32_t start=(cSkipBytes.flags()&CmdArg::GIVEN?cSkipBytes:0);
  if(cVerGain.flags()&CmdArg::GIVEN){ wave->SetVerticalGain(cVerGain); }
  if(cVerOffset.flags()&CmdArg::GIVEN){ wave->SetVerticalOffset(cVerOffset); }

  cout << "Unpack: " << bytes.size() << " bytes" << endl;
  //wave->Decode(&bytes[start],bytes.size()-start);

  delete wave;

  return 0;
}
