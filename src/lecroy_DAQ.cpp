/***************************************************************************
lecroy_example.c
Version 1.00
Copyright (C) 2003  Steve D. Sharples

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

The author's email address is steve.sharples@nottingham.ac.uk

***************************************************************************

Example program that demonstrates communication via TCP/IP with an ethernet-
enabled LeCroy DSO. Based on the main() function of "net_con.cpp" written
by LeCroy.

See "lecroy_tcp.c" and "lecroy_tcp.h" for additional comments and usage

**************************************************************************/


#include "lecroy_tcp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/statvfs.h>
#include <signal.h> 
#include <string>
#include <iostream>
#include <fstream>
#include <ctime>
#include <algorithm>
using namespace std;

#define	MAX_ST	516384	/* maximum message string. no picked from thin air */
#define RAW_DATA_ENABLED
#define MAX_SEG 5000
//#define WITH_BUSY
//#define WITH_DEBUG

volatile sig_atomic_t flag = 0;

static char buf[516384];
static unsigned char dataBuf[516384];
static unsigned char headbuf[8];
//static char datbuf[16384];
static int sockfd;


void getDiskSpace()
{
    struct statvfs fiData;

    if((statvfs(".",&fiData)) < 0 ) {
            cout << "\nFailed to get disk info"  << endl;
    } else {
            float avSpace = 100*(double)(fiData.f_bavail*fiData.f_frsize/(double)(fiData.f_blocks*fiData.f_frsize));
            printf("=====================================\n");
            printf("Available disk space: %11.2lf %% \n", avSpace);       
            printf("=====================================\n");
            if(avSpace<1)
            {
             
              printf("ONLY 1%% DISK SPACE REMAINING - YOU SHALL NOT PASS!!!\n");
              printf("Please contact the coordinators to free up some space for you \n");
              exit(EXIT_FAILURE);
            }

    }
    
}

void signalHandler_inWrite(int sig){ 
  flag = 1;
  cout << "ctrl-C caught in data transfer - press again when data transfer is complete..."  << endl;
}

void signalHandler_inTrigger(int sig){
  cout << "ctrl-C caught - will stop now! " << endl;
  cout << "Please stop data acquisition in scope (Top menu --> Trigger --> Stop) !!! " << endl;
  snprintf(buf, sizeof(buf), "$$SY_FP 33,1\n"); //"STOP BUTTON" command
  LECROY_TCP_write(sockfd, buf);
  printf ("Request to scope:\n%s\n", buf);  

  close(sockfd);
  LECROY_TCP_disconnect(sockfd);
  

  exit(0);
 

}

//void signalHandler_inWaitOrSpill(int sig)
//{
//  flag = 1;
//  break;
//}
//void signalHandler_inWaitOrSpill(int )

int scope_readHeader(int &blockSize, bool &eoiTerminated, bool &srqStateChanged, int &seqNum){

    int recvHeaderLen = 0;
    //fd_set readSet = { 1, { m_socketFd } }; 
    //TIMEVAL timeVal = { (long) m_currentTimeout, ((long) (m_currentTimeout * 1000000L)) % 1000000L};

    // if there is no sign of a header, get out quick (timeout)
      fd_set rfds;
      struct timeval tval;

      tval.tv_sec = MAX_TCP_READ;
      tval.tv_usec = 0;

      //if (LECROY_TCP_connected_flag != TRUE) return -1;

      FD_ZERO(&rfds);
      FD_SET(sockfd, &rfds);
    // wait until 8 bytes are available (the entire header)
      int headerBytesRead = 0, bytesThisTime;
      while(headerBytesRead < 8)
      {
        // ensure that the recv command will not block
          int numReady = select((sockfd+1), &rfds, NULL, NULL, &tval);
          if(numReady != 1)
            break;

        // try to read the remainder of the header
          bytesThisTime = recv(sockfd, (char *) headbuf + headerBytesRead, HEADER_SIZE - headerBytesRead, 0);
          if(bytesThisTime > 0)
            headerBytesRead += bytesThisTime;

        // if we get this far without reading any part of the header, get out
          if(headerBytesRead == 0)
            break;
      }

    // receive the scope's response, header first 
      if(headerBytesRead == 8)
      {
        // extract the number of bytes contained in this packet
          blockSize = ntohl(*((unsigned long *) &headbuf[4]));
          int blockSize_MSB = ntohl(*((unsigned long *) &headbuf[7]));

          // check the integrity of the header
          if(!((headbuf[0] & LECROY_DATA_FLAG) && (headbuf[1] == HEADER_VERSION1) ))
          {
            printf("Invalid Header!\n");

            return(-1);
          }

        // inform the caller of the EOI and SRQ state
          if(headbuf[0] & LECROY_EOI_FLAG)
            eoiTerminated = TRUE;
          else
            eoiTerminated = false;

          if(headbuf[0] & LECROY_SRQ_FLAG)
            srqStateChanged = TRUE;
          else
            srqStateChanged = false;

        // inform the caller of the received sequence number
          seqNum = ntohl(*((unsigned long *) &headbuf[2]));
          if(seqNum!=0)
          {
            cout << "problem?" << endl;
          }
          //seqNum = headbuf[2];

        //printf("readHeaderFromDevice: seq=%d\n",seqNum);
      }
      else
      {

        return(-1);
      }
}


int scope_data2disk(int id_dat,int sockfd,int nbytes){
  //signal(SIGINT, signalHandler_inWrite);
  int nrec;
  int nrectot=0;
  int nhdr = 0;
  int ntrl = 0;
  

  int blockSize;
  bool eoiTerminated;
  bool srqStateChanged;
  int seqNum;
  int firsthead=16;
  //while(1)
  //{
   
  //}

  int cntSegs=0;
  while (nbytes > 0){
      int nhead=scope_readHeader(blockSize, eoiTerminated, srqStateChanged, seqNum);
      if(nhead==-1)
      {
        cout << "header not found or invalid!" << endl;
        break; 
      }
      //if(srqStateChanged==1)
      //cout << "blockSize = " << blockSize <<"  srqStateChanged?  " <<  srqStateChanged << "  eoiTerminated?  " << eoiTerminated << endl;  
      //if(eoiTerminated==1)
      //  break;
      int mycnt=0;
      int nrec_in_block=0;


      while (nrec_in_block<blockSize)
      { 

        memset(dataBuf,sizeof(dataBuf),0);

        
        //int nrec_thistime = recv(sockfd, dataBuf, blockSize-nrec_in_block, 0);

        //int nrec_thistime = recv(sockfd, (char *) dataBuf+nrec_in_block, blockSize-nrec_in_block, 0);
        int nrec_thistime=recv(sockfd, dataBuf, blockSize-nrec_in_block, 0);
        if(nrec_thistime > 0)
          nrec_in_block += nrec_thistime;

        if(blockSize==5 or blockSize==11 or blockSize==3)
             continue;
        //cout << blockSize << "   " << nrec_thistime << "   " << nrec_in_block << endl;
        //
        //if(blockSize==1){
        //  short s =ntohs(*((unsigned short *) &dataBuf[0]));
        //  cout << s << endl;
        //  cout << dataBuf[0] << "   " << dataBuf[1] << endl;
        //   dataBuf[0]='0';
        //   dataBuf[1]='0';
        //   dataBuf[2]='0';
        //   dataBuf[3]='0';
//
        //}
        if (id_dat > 0) //write(id_dat, buf, nrec);
          write(id_dat,dataBuf,nrec_thistime);
        firsthead=0;

 
        //if(dataBuf[0] & LECROY_LOCKOUT_FLAG)
          //cout << "lockout?" << endl;
           // for (const char* p = dataBuf; *p; ++p)
           //{
           //     printf("%02x", *p);
           // }
           //     cout << endl;
      //}
          if(nrec_in_block <= 0)
            break;

      }
      
      cntSegs+=1;

      
      //if (id_dat > 0) //write(id_dat, buf, nrec);
      //    write(id_dat,dataBuf, nrec_in_block);
      //write(id_dat,(char*) dataBuf, nrec_thistime);    
      //if (id_dat > 0) //write(id_dat, buf, nrec);
      //        write(id_dat,(char*) dataBuf, nrec_in_block);
      //nrec = recv(sockfd, buf, sizeof(buf), 0);
      //nrec = recv(sockfd, buf, blockSize, 0);
      //if (nrec <= 0)
      //  break;
      
      nrectot+=nrec_in_block;
      //cout << nbytes << "   " << nrec_in_block << "   " << nrectot << endl;
      nbytes -= nrec_in_block;
      ntrl = (nbytes == 0);

  }
  //cout << nbytes << "   " << eoiTerminated << endl;
  if (nbytes != 0 || eoiTerminated!=1)//||re buf[nrec-1] != '\n')
    return 1;
  else
    return 0;
}





int swrite(char* buf){
  int len = strlen(buf);
  int val;
  val = send(sockfd, buf, len, 0);
  if (val <= 0){
  	cout << "problem" << endl;
    perror(buf);
    return 0;
  }
  return val;
}

int sendWrite(char *inbuf,char *outbuf,int sockfd,char* command){
  strcpy(outbuf,command);
  LECROY_TCP_write(sockfd, outbuf);

  printf ("Request to scope:\n%s\n", outbuf);

  //printf ("Write result: %d\n", LECROY_TCP_write(sockfd, buf));
  memset(outbuf,0,sizeof(outbuf));
  memset(inbuf,0,sizeof(inbuf));
  /* example of getting the raw data back from the scope */
  int result=LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
  printf ("Scope returned:\n%s\n", inbuf);
  memset(inbuf,0,sizeof(inbuf));

  return result;
}






int main (int argc, char** argv)
{
        signal(SIGINT,signalHandler_inTrigger);

	typedef basic_fstream<char> fstream;

  char ip_address[50];
  static char outbuf[MAX_ST];
  char inbuf[MAX_ST];
  float fvalue;
  int ivalue;
  std::vector<int> activeChans;
  struct timeval tv;
  double sps_cycle = 0.;
  int sps_cycle_sec;
  int sps_cycle_usec;

  time_t      nxt_sec;
  suseconds_t nxt_usec;
  time_t      tnow;

  int id_dat = 0;
  int id_txt = 0;
  int id_trig = 0;
  int ier;
  int nsegm, nsegmtotal=0;
  int nbytes, nel;
  int i;
  int nchannels = 4;
  int chmask    = 0;

  int pre_fmt;
  int pre_type;
  int pre_npts;

  sigset_t signalMask;
  sigset_t blockedSignals;
  /* Fine time stamp is also used to synchronize with SPS cycle */
  gettimeofday(&tv, NULL);
  nxt_sec  = tv.tv_sec;
  nxt_usec = tv.tv_usec;

  int    scl;
  double eps;
  sps_cycle=30;
  scl = (int)(sps_cycle / 1.2 + 0.5);
  eps = sps_cycle - 1.2 * scl;
  sps_cycle_sec  = sps_cycle;
  sps_cycle_usec = (10.*sps_cycle + 0.5) - 10 * sps_cycle_sec;
  sps_cycle_usec = sps_cycle_usec * 100000;
  if (sps_cycle < 10.0 || eps < -1.0E-3 || eps > 1.0E-3){
      fprintf(stderr, "%s does not seem a correct supercycle length\n",
        argv[1]);
      return 1;
    }
  
activeChans.clear();

  getDiskSpace();
#ifdef RAW_DATA_ENABLED
  /* Open Binary output data file */
  ofstream datFile;
  snprintf(inbuf, sizeof(inbuf), "Data/data_%ld.dat", nxt_sec);
  id_dat = open(inbuf, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  memset(inbuf,0,sizeof(inbuf));
  if (id_dat < 0){
    perror(inbuf);
    return 1;
  }
#endif

  /* Open ASCII output data (meta) file */
  snprintf(buf, sizeof(buf), "Data/data_%ld.txt", nxt_sec);
  id_txt = open(buf, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (id_txt < 0){
    perror(buf);
    return 1;
  }
  /* Open ASCII output triggers file */
  snprintf(buf, sizeof(buf), "Data/trigs_%ld.txt", nxt_sec);
  id_trig = open(buf, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (id_trig < 0){
    perror(buf);
    return 1;
  }
  /* OUR_SCOPE_IP is defined in lecroy_tcp.h, so is MAX_TCP_CONNECT */
  sprintf(ip_address, OUR_SCOPE_IP);

  printf("\nlecroy_example: about to connect to DSO...\n");



  /* how to "set up the scope for ethernet communication" */
  sockfd=LECROY_TCP_connect(ip_address, MAX_TCP_CONNECT);
  if (sockfd<0) {
  	printf("\nCould not connect to the scope on IP: %s\n",ip_address);
  	return 1;
  	}

	printf("....connected.\n");


    strcpy(outbuf,"*IDN?\n");
    LECROY_TCP_write(sockfd, outbuf);
    printf ("Request to scope:\n%s\n", outbuf);
    LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
    printf("Command returned:%s", inbuf);
    write(id_txt, inbuf, string (inbuf).length());


   //strcpy(outbuf,"SEQ ON,10,6400\n");     //remove if setting number of segments manually
   //LECROY_TCP_write(sockfd, outbuf);
   //printf ("Request to scope:\n%s\n", outbuf);

   strcpy(outbuf,"SEQ?;WAIT;*OPC?\n");
   LECROY_TCP_write(sockfd, outbuf);

    #ifdef WITH_DEBUG
        printf ("Request to scope:\n%s\n", outbuf);
    #endif

   LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);

    #ifdef WITH_DEBUG
   printf("Command returned:%s", inbuf);
    #endif


   strcpy(outbuf,"COMM_HEADER OFF;WAIT;*OPC?\n");
   LECROY_TCP_write(sockfd, outbuf);

  #ifdef WITH_DEBUG
   printf ("Request to scope:\n%s\n", outbuf);
  #endif

   LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
  
    #ifdef WITH_DEBUG
   printf("Command returned:%s", inbuf);
    #endif

   //strcpy(outbuf,"CFMT OFF,BYTE,BIN;WAIT;*OPC?\n");    //setting communication format: byte,binary
   strcpy(outbuf,"CFMT DEF9,WORD,BIN;WAIT;*OPC?\n");    //setting communication format: byte,binary
   LECROY_TCP_write(sockfd, outbuf);

       #ifdef WITH_DEBUG
   printf ("Request to scope:\n%s\n", outbuf);
       #endif
   LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
  
    #ifdef WITH_DEBUG
   printf("Command returned:%s", inbuf);   
    #endif
  
   strcpy(outbuf,"CORD LO;WAIT;*OPC?\n");    //lower significance bit fitst
   LECROY_TCP_write(sockfd, outbuf);
  
    #ifdef WITH_DEBUG
   printf ("Request to scope:\n%s\n", outbuf);
    #endif
   LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
   
    #ifdef WITH_DEBUG
   printf("Command returned:%s", inbuf);
    #endif
   //activeChans.push_back(2);
   //activeChans.push_back(3);
   
    for(i=0; i<nchannels; i++){
  

         snprintf(outbuf, sizeof(outbuf), "C%d:TRA?;WAIT;*OPC?\n", i+1);  //active channels - set manually if you find it is too slow!
         LECROY_TCP_write(sockfd, outbuf);
  
          #ifdef WITH_DEBUG
         printf ("Request to scope:\n%s\n", outbuf);
          #endif
         //memset(outbuf, 0, sizeof(outbuf));
  
         LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
         
          #ifdef WITH_DEBUG
         printf("Command returned:  %s\n", inbuf);
          #endif
  
         if(string(inbuf).find("OFF")== string::npos)
         {
            chmask |= (1 << i);
            snprintf(outbuf,sizeof(outbuf), "\nChannel %d will be saved\n", i+1);
            string stChan(outbuf);
            write(id_txt,outbuf,stChan.length());
            
            cout << outbuf << endl;

          //activeChans.push_back(i+1);


         }
     }
//     memset(outbuf,sizeof(outbuf),0);

      #ifdef WITH_BUSY
        snprintf(outbuf,sizeof(outbuf),"COUT PULSE, 0.05 V;*OPC?\n");
        LECROY_TCP_write(sockfd, outbuf);
      #ifdef WITH_DEBUG
	    printf ("Request to scope:\n%s\n", outbuf);
      #endif

	    LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
	   
      #ifdef WITH_DEBUG   
      printf("Command returned:%s", inbuf);
      #endif
      #endif



     sigemptyset(&signalMask);
     sigaddset(&signalMask,SIGINT);
     
     sigemptyset(&blockedSignals);

    //=========HERE GOES THE CYCLE LOOP========//
     int icycle=0;
     int NbCycles=20;
     int in_spill=5; //secs of spill duration - arbitrary for now
     int breakFlag=0;
     int nsegTot=0;
     while(1)
     {

      int nsegInCycle=0;
      if(breakFlag==1)
      {
      cout << "incomplete data transfer - will exit the program" << endl;
       break;
      }

     //if CTRL-C has been received during data acquisition - stop here!
      if(sigpending(&blockedSignals)<0){
       perror("sigpending()");
       break;
     }
      if(sigismember(&blockedSignals,SIGINT)==1) {
       fputs("\n CTRL-C received during data taking - executing now that data readout is complete!\n",stderr); 
       break;    
     }
     
    //Unblock CTRL-C signal 
    if (sigprocmask(SIG_UNBLOCK,&signalMask,NULL)<0)
       perror("sigprocmask()");


      signal(SIGINT,signalHandler_inTrigger);
      cout << "\n I'm ready for triggers! \n" << endl;

      //signal(SIGINT,signalHandler_inTrigger);

      strcpy(outbuf,"TRMD SINGLE;WAIT;*OPC?\n"); //could be replaced by *TRG/TRMD SINGLE
      LECROY_TCP_write(sockfd, outbuf);

      #ifdef WITH_DEBUG
      printf ("Request to scope:\n%s\n", outbuf);
      #endif

      LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);

      #ifdef WITH_DEBUG
      printf("Command returned:%s", inbuf);
      #endif

    

      
      time_t tstart = time(NULL);




    //=================WAVEFORM ACQUISITION=============//
    //=== time to be replaced by sps-cycle  ==//
    //==================================================//
      //while((time(NULL)-tstart)<in_spill)      
      //{
      //  
      //}          



      //snprintf(outbuf, sizeof(outbuf), "$$SY_FP 33,1\n"); //"STOP BUTTON" command
      snprintf(outbuf, sizeof(outbuf), "STOP;WAIT;*OPC?\n"); //"STOP BUTTON" command
      LECROY_TCP_write(sockfd, outbuf);

      #ifdef WITH_DEBUG
      printf ("Request to scope:\n%s\n", outbuf);
      #endif
      //LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
      //sprintf("Command returned:%s", inbuf);
      LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);

      #ifdef WITH_DEBUG
      printf("Command returned:%s", inbuf);
      #endif

      //Block CTRL-C signal during data readout
      if(sigprocmask(SIG_BLOCK,&signalMask,NULL)<0)
        perror("sigprocmask()");
                  


     //emit busy signal
     // #ifdef WITH_BUSY
     //       snprintf(outbuf,sizeof(outbuf),"COUT LEVEL, 0.05 V;*OPC?\n");
     //       LECROY_TCP_write(sockfd, outbuf);
     // #ifdef WITH_DEBUG
     //       printf ("Request to scope:\n%s\n", outbuf);
     // #endif

     //       LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);

     // #ifdef WITH_DEBUG
     //       printf("Command returned:%s", inbuf);
     // #endif
     // #endif
	 




    //============================================//
    //=================DATA TRANSFER =============//
    //============================================//
    for(i=0; i<nchannels; i++){
     
     if ((chmask & (1<<i)) == 0) continue;
   		  

   		    snprintf(outbuf,sizeof(outbuf),"C%d:INSP? 'NOM_SUBARRAY_COUNT';WAIT;*OPC?\n",i+1);  
          LECROY_TCP_write(sockfd, outbuf);
          
          #ifdef WITH_DEBUG
          printf ("Request to scope:\n%s\n", outbuf);
          #endif
          LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
          
          #ifdef WITH_DEBUG
          printf("Command returned:%s", inbuf);
          #endif

          string str(inbuf);
          string str2=str.substr((size_t) str.find(": ")+2);
          int cnt=0;
          
          for(int i=0;i<4;i++)
          {
            if(str2[i]!=' ' && str2[i]!='"')
              cnt++;
          }
          #ifdef WITH_DEBUG
          cout << "Number of acquired waveforms in cycle " << icycle << " is = " << str2.substr(0,cnt) << endl;
          #endif


	        snprintf(outbuf,sizeof(outbuf),"C%d:INSP? 'WAVE_ARRAY_COUNT';WAIT;*OPC?\n",i+1);   
          LECROY_TCP_write(sockfd, outbuf);

          #ifdef WITH_DEBUG
      	  printf ("Request to scope:\n%s\n", outbuf);
          #endif

      	  LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);

          #ifdef WITH_DEBUG
      	  printf("Command returned:%s", inbuf);
          #endif

      	  string str3(inbuf);
      	  string str4=str3.substr((size_t) str3.find(": ")+2);
      	  int cnt1=0;
      	  for(int i=0;i<str3.length();i++)
      	  {
      	    if(str4[i]!=' ' && str4[i]!='"')
      	      cnt1++;
      	  }
          #ifdef WITH_DEBUG
      	  cout << "Number of points in cycle " << icycle << " is = " << str4.substr(0,cnt1) << endl;
          #endif
        // Waveform data goes to binary file //

         int nseg=stoi(str2.substr(0,cnt));
         nsegInCycle=nseg;
    	   int npoints=stoi(str4.substr(0,cnt1))/nseg;
         int hdr=8;   //8-bit header of Lecroy VCIP protocol in every block
         int firstHdr=16+3*hdr; //DAT1 header info
         int trl=3;
       
         int expected_bytes=2*nseg*npoints+19;
         //int expected_bytes=nseg*npoints+19;

         snprintf(outbuf, sizeof(outbuf), "C%d:WF? DAT1;WAIT;*OPC?\n",i+1);    //get waveforms
         nel=LECROY_TCP_write(sockfd, outbuf);

         #ifdef WITH_DEBUG
    	   printf ("Request to scope:\n%s\n", outbuf);
         #endif




          ier = scope_data2disk(id_dat,sockfd,expected_bytes);

          if(ier==1)
          {
            breakFlag=1;
          	break;
          }

          //snprintf(outbuf, sizeof(outbuf), "C%d:INSP? 'WAVEDESC';WAIT;*OPC?",i+1);
          //snprintf(outbuf, sizeof(outbuf), "C%d:INSP? 'WAVE_ARRAY_1';WAIT;*OPC?;C%d:INSP? 'NOM_SUBARRAY_COUNT';WAIT;*OPC?;C%d:INSP? 'HORIZ_INTERVAL';WAIT;*OPC?;C%d:INSP? 'HORIZ_OFFSET';WAIT;*OPC?;C%d:INSP? 'VERTICAL_GAIN';WAIT;*OPC?;WAIT;*OPC?\n",i+1,i+1,i+1,i+1,i+1,i+1);
          snprintf(outbuf, sizeof(outbuf), "C%d:INSP? 'WAVE_ARRAY_1';C%d:INSP? 'NOM_SUBARRAY_COUNT';C%d:INSP? 'HORIZ_INTERVAL';C%d:INSP? 'HORIZ_OFFSET';C%d:INSP? 'VERTICAL_OFFSET';C%d:INSP? 'VERTICAL_GAIN';C%d:INSP? 'TRIGGER_TIME';WAIT;*OPC?",i+1,i+1,i+1,i+1,i+1,i+1,i+1);//C%d:INSP? 'NOM_SUBARRAY_COUNT';C%d:INSP? 'HORIZ_INTERVAL';C%d:INSP? 'HORIZ_OFFSET';C%d:INSP? 'VERTICAL_GAIN';C%d:INSP? 'VERTICAL_OFFSET';WAIT;*OPC?\n",i+1,i+1,i+1,i+1,i+1,i+1);
          //snprintf(outbuf, sizeof(outbuf), "WAIT;C%d:INSP? 'INSTRUMENT_NAME';*OPC?",i+1);
          nel=LECROY_TCP_write(sockfd, outbuf);

          #ifdef WITH_DEBUG
          printf ("Request to scope:\n%s\n", outbuf);
          #endif

          LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
          string st(inbuf);
          write(id_txt, inbuf, st.length());

 	      snprintf(outbuf, sizeof(outbuf), "C%d:INSP? TRIGTIME;WAIT;*OPC?\n",i+1);    //get waveforms
          nel=LECROY_TCP_write(sockfd, outbuf);
          int expectedTrigBytes=nsegInCycle*40+66;//2*2*nsegInCycle*npoints*8+19;
          ier = scope_data2disk(id_trig,sockfd,expectedTrigBytes);


          if(ier==1)
          {
            breakFlag=1;
          	break;
          }
    }
    
     //emit busy signal
     #ifdef WITH_BUSY
            snprintf(outbuf,sizeof(outbuf),"COUT LEVEL, 0.95 V;*OPC?\n");
            LECROY_TCP_write(sockfd, outbuf);
      #ifdef WITH_DEBUG
            printf ("Request to scope:\n%s\n", outbuf);
      #endif

            LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);

      #ifdef WITH_DEBUG
            printf("Command returned:%s", inbuf);
      #endif
      #endif

      #ifdef WITH_BUSY
        snprintf(outbuf,sizeof(outbuf),"COUT PULSE, 0.05 V;*OPC?\n");
        LECROY_TCP_write(sockfd, outbuf);
      #ifdef WITH_DEBUG
	    printf ("Request to scope:\n%s\n", outbuf);
      #endif

	    LECROY_TCP_read(sockfd, inbuf, sizeof(inbuf), MAX_TCP_READ);
	   
      #ifdef WITH_DEBUG   
      printf("Command returned:%s", inbuf);
      #endif
      #endif




    nsegTot+=nsegInCycle;
    cout << "===============================" << endl;
    cout << "Number of acquired triggers in this cycle = " << nsegInCycle << ", in total = " << nsegTot << endl;
    cout << "===============================" << endl;

    //snprintf(outbuf, sizeof(outbuf), "WAIT;C%d:INSP? 'TRIGTIME'",activeChans[0]);

      icycle++;
  }


  snprintf(outbuf, sizeof(outbuf), "STOP\n");
  LECROY_TCP_write(sockfd, outbuf);
  printf ("Request to scope:\n%s\n", outbuf);  

  close(sockfd);
  LECROY_TCP_disconnect(sockfd);
  printf("Successfully disconnected... bye!\n");
  return 0;
}

