#include <iostream>
#include <iomanip>
#include <signal.h>
#include <chrono>
#include <cmdl/cmdargs.h>

#include "LecroyHGTD/LecroyHGTD.h"
#include "LecroyHGTD/LecroyHGTDData.h"
#include "LecroyHGTD/LecroyHGTDTree.h"
 
using namespace std;

bool g_cont;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# test_LecroyHGTD               #" << endl
       << "#################################" << endl;

  CmdArgBool   cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr   cFilePath('f',"file","path","file to parse", CmdArg::isREQ);
  CmdArgStr    cAddress('a',"address","ip-address","ip-address", CmdArg::isREQ);
  CmdArgInt  cRunNumber('r',"run","number","runnumber");
  
  CmdLine cmdl(*argv,&cVerbose,&cAddress,&cFilePath,&cRunNumber,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  cout << "Create new LecroyHGTD" << endl;
  LecroyHGTD *lecroy=new LecroyHGTD();

  cout << "Set verbose mode" << endl;
  lecroy->SetVerbose(cVerbose);

  cout << "Connect to scope" << endl;
  lecroy->Connect(string(cAddress));

  cout << "Configure scope" << endl;
  lecroy->Configure();

  cout << "Create the Tree" << endl;
  LecroyHGTDTree *ntuple=new LecroyHGTDTree();

  uint32_t runnumber=(cRunNumber.flags()&CmdArg::GIVEN?cRunNumber:666);
  cout << "Set Run number: " << runnumber << endl;
  ntuple->SetRunNumber(runnumber);

  cout << "Open the ntuple" << endl;
  ntuple->Open(string(cFilePath),"RECREATE");
  
  g_cont=true;
  signal(SIGINT,handler);
  signal(SIGTERM,handler);
  lecroy->SetAbortFlag(&g_cont);

  uint32_t eventid=0;
  cout << "Loop" << endl;
  while(g_cont){
    cout << "Start" << endl;
    lecroy->Start();
    cout << "Stop" << endl;
    lecroy->Stop();
    cout << "Read" << endl;
    lecroy->Read();
    if(!g_cont) break; 
    cout << "Extract the events" << endl;
    auto t0 = std::chrono::steady_clock::now(); 
    for(uint32_t event=0;event<lecroy->MAX_NUM_EVENTS;event++){
      bool fill=false;
      for(auto& chan: lecroy->GetEnabledChannels()){
	if(event==0) cout << "Chan: " <<chan << ", NumEvents: " << lecroy->GetNomSubArrayCount(chan) << endl;	
	if(event>lecroy->GetNumEvents(chan)) continue;
	ntuple->Get(chan)->SetEventId(eventid);
	lecroy->Fill(ntuple->Get(chan),chan,event);
	fill=true;
      }
      if(!fill) continue;
      ntuple->Fill();
      eventid++;
      //if((eventid+1)%32==0)cout << "eventid: " << eventid << endl;
    }
    auto t1 = std::chrono::steady_clock::now(); 
    cout << "Filling took " 
	 << chrono::duration_cast<chrono::milliseconds>(t1-t0).count()
	 << " ms" << endl;
  }

  cout << "Close the ntuple" << endl;
  ntuple->Close();
  
  cout << "Cleaning the house" << endl;
  delete ntuple;
  delete lecroy;

  cout << "Have a nice day" << endl;
  return 0;
}
